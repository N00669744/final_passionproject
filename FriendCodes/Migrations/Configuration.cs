namespace FriendCodes.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using FriendCodes.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<FriendCodes.Models.FriendCodeContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(FriendCodes.Models.FriendCodeContext context)
        {
            context.Users.AddOrUpdate(u => u.User_ID,
                new User
                {
                    User_IGN = "Ikon",
                    Friend_Code = "123456789",
                    UserActive = 0
                }
                );
        }
    }
}
