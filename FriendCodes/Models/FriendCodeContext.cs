﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace FriendCodes.Models 
{
    public class FriendCodeContext : DbContext
    {
        public FriendCodeContext()
        {
            
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<Available_Unit> Available_Units { get; set; }
        public DbSet<Desired_Unit> Desired_Units { get; set; }
    }
}