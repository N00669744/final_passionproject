﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;


/*** Example input: Lead(s):** 1800 ATK 6★ Enhanced Olive*/
namespace FriendCodes.Models
{
    public class Available_Unit
    {
        [Key,ScaffoldColumn(false)]
        public int AvailableUnit_ID { get; set; }

        [Required, Range(6, 7), Display(Name = "Rarity")]
        public int Rarity { get; set; }

        [StringLength(255), Display(Name = "Build")]
        public string Build { get; set; }

        [Required]
        public int? Unit_ID { get; set; }

        [StringLength(255), Display(Name = "Unit Name")]
        public string Unit_Name { get; set; }

        //Units Owner
        public virtual User User { get; set; }
        public virtual ICollection<Unit> Units { get; set; }
    }
}